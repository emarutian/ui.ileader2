const functions = require('firebase-functions');
const nodemailer = require('nodemailer');

exports.sendInvitationEmail = functions.https.onCall((data, context) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'savemypetmail@gmail.com',
      pass: 'savemypet'
    }
  });

  const email = data.email;
  const mailOptions = {
    to: email,
    from: 'savemypetmail@gmail.com',
    subject: 'You have been invited to better-leader.me',
    text: 'You have been invited to join better-leader.me platform https://better-leader.me'
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      console.log('Error sending email ...', err)
      return {
        success: false,
        message: `Error sending invitation email to ${email}`,
        err
      }
    } else {
      console.log('Invitation sent successfully', info)
      return {
        success: true,
        message: `Invitation Email sent successfully to ${email}`,
        info
      }
    }
  });
});
