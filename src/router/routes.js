import { AUTH } from 'src/plugins/firebase'
import store from 'src/store'
import settings from '../store/settings'

function checkAuth (next, loginRoute, logoutRoute) {
  AUTH.onAuthStateChanged(user => {
    if (user) {
      store().dispatch('login', user)
      if (loginRoute) { next(loginRoute) } else { next() }
    } else {
      if (logoutRoute) { next(logoutRoute) } else { next() }
    }
  })
}

function allowOnAuth (to, from, next) {
  let userLoggedIn = Boolean(store().state.user)
  if (userLoggedIn) {
    next()
  } else {
    checkAuth(next, '', '/')
  }
}

function preventOnAuth (to, from, next) {
  let userLoggedIn = Boolean(store().state.user)
  if (userLoggedIn) {
    next('/dashboard')
  } else {
    checkAuth(next, '/dashboard', '')
  }
}

function guardLandingPage (to, from, next) {
  const userLoggedIn = Boolean(store().state.user)
  const isFromInside = Boolean(from.name)
  if (isFromInside) {
    next()
  } else if (userLoggedIn) {
    next('/dashboard')
  } else {
    checkAuth(next, '/dashboard', '')
  }
}

function loadPage (page) {
  return () => import(`pages/${page}`)
}

function loadLayout (layout) {
  return () => import(`layouts/${layout}`)
}

const routes = [
  {
    path: '/',
    component: loadLayout('default'),
    children: [
      {
        path: '/',
        name: '',
        component: loadPage('About'),
        beforeEnter: guardLandingPage
      },
      {
        path: '/login',
        name: settings.site.name,
        component: loadPage('Login'),
        beforeEnter: preventOnAuth
      },
      /* {
        path: '/view/playbook/:code',
        name: 'Org Playbook',
        component: loadPage('ViewPlaybook'),
        props: true
      }, */
      {
        path: '/dashboard',
        name: 'My Dashboard',
        component: loadPage('Dashboard'),
        beforeEnter: allowOnAuth
      },
      {
        path: '/settings',
        name: 'Settings',
        component: loadPage('Settings'),
        beforeEnter: allowOnAuth
      },
      {
        path: '/dashboard',
        name: 'Team Dashboard',
        component: loadPage('Dashboard'),
        beforeEnter: allowOnAuth
      },
      {
        path: '/projects',
        name: 'Team Projects',
        component: loadPage('Projects'),
        beforeEnter: allowOnAuth
      },
      {
        path: '/playbook',
        name: 'Org Playbook',
        component: loadPage('Playbook'),
        beforeEnter: allowOnAuth
      },
      {
        path: '/home',
        name: 'Home',
        component: loadPage('Home'),
        beforeEnter: allowOnAuth
      },
      {
        path: '/org',
        name: 'My Org',
        component: loadPage('Org'),
        beforeEnter: allowOnAuth
      },
      {
        path: '/team/:id',
        name: 'Team',
        component: loadPage('Team'),
        beforeEnter: allowOnAuth,
        props: true
      },
      {
        path: '/myProfile',
        name: 'My Profile',
        component: loadPage('Profile'),
        props: true,
        beforeEnter: allowOnAuth
      },
      {
        path: '/profile/:id',
        name: 'Profile',
        component: loadPage('Profile'),
        props: true,
        beforeEnter: allowOnAuth
      }
    ]
  },
  {
    path: '/view',
    component: loadLayout('PlaybookLayout'),
    children: [
      {
        path: 'playbook/:code',
        name: 'Org Playbook',
        component: loadPage('ViewPlaybook'),
        props: true
      }
    ]
  },
  { path: '*', component: loadPage('Error404') } // Not found
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: loadPage('Error404.vue')
  })
}

export default routes
