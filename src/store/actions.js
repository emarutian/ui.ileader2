import router from 'src/router'
import { AUTH, DB } from 'src/plugins/firebase'
import { firebaseAction } from 'vuexfire'

export function login ({ state, dispatch }, user) {
  state.user = user
  dispatch('createUserDoc')
  dispatch('createDefaultOrg')
  dispatch('loadInitialData')
}

export function logout ({ state }) {
  state.user = null
  AUTH.signOut()
    .then(() => {
      router().push('/')
    })
    .catch(e => {
      console.log(e)
    })
}

// create user instance if not already created
export async function createUserDoc ({ state }) {
  const userDocRef = DB.doc(`users/${state.user.uid}`)
  const userDoc = await userDocRef.get()
  if (!userDoc.exists) {
    userDocRef.set({
      email: state.user.email
    })
  }
}

// create default Org if not already created
export async function createDefaultOrg ({ state }) {
  const orgId = `org${state.user.uid}`
  const orgDocRef = DB.doc(`orgs/${orgId}`)
  const orgDoc = await orgDocRef.get()
  if (!orgDoc.exists && state.user.uid) {
    console.log('[actions] creating the default Org')
    orgDocRef.set(Object.assign({}, state.models.org, { ownerId: state.user.uid }))
  }
}

// Initial data loading
export async function loadInitialData ({ dispatch, state }) {
  dispatch('loadUserProfile')
  dispatch('loadOrg')
  dispatch('loadTeams')
}

// General CRUD actions
export async function deleteDoc ({ dispatch, state }, payload) {
  console.log(`[actions] Delete Doc: ${payload.id} from collection: ${payload.collection}`)
  DB.doc(`${payload.collection}/${payload.id}`).delete()
}

export async function updateDoc ({ dispatch, state }, payload) {
  console.log(`[actions] Update Doc: ${payload.item.id} in collection: ${payload.collection}`)
  DB.doc(`${payload.collection}/${payload.item.id}`).update(payload.item)
}

export function updateMultipleDocs ({ state }, payload) {
  console.info(`[Actions] Update multiple docs in collection ${payload.collection}`)
  let batch = DB.batch()
  payload.items.forEach(item => {
    let docRef = DB.doc(`${payload.collection}/${item.id}`)
    batch.update(docRef, item)
  })
  batch.commit()
}

export const loadUserProfile = firebaseAction(({ bindFirebaseRef, state }) => {
  console.log('[actions] Loading current user profile')
  bindFirebaseRef('userProfile', DB.doc(`users/${state.user.uid}`))
})

export const loadOrg = firebaseAction(async ({ bindFirebaseRef, state }) => {
  console.log('[actions] Loading default Org')
  bindFirebaseRef('org', DB.doc(`orgs/org${state.user.uid}`))
})

export const updateOrg = firebaseAction(async ({ bindFirebaseRef, state }, org) => {
  console.log('[actions] Update default Org')
  DB.doc(`orgs/org${state.user.uid}`).update(org)
})

export const loadTeams = firebaseAction(async ({ bindFirebaseRef, state }) => {
  console.log('[actions] teams loaded')
  await bindFirebaseRef(
    'teams',
    DB.collection(`users/${state.user.uid}/teams`).orderBy('createdAt', 'desc')
  )
  state.teamsFetchCompleted = true
})

export const loadMembersByTeamId = firebaseAction(({ bindFirebaseRef, state }, teamId) => {
  console.log(`[actions] loading Members of team: ${teamId}`)
  return bindFirebaseRef(
    'members',
    DB
      .collection(`users/${state.user.uid}/teams/${teamId}/members`)
      .orderBy('createdAt', 'desc')
  )
})

export const loadGoals = firebaseAction(({ bindFirebaseRef }, ref) => {
  console.log('[actions] Goals ref set')
  bindFirebaseRef('goals', ref)
})

export const loadStrengths = firebaseAction(({ bindFirebaseRef }, ref) => {
  console.log('[actions] Strengths ref set')
  bindFirebaseRef('strengths', ref)
})

export const loadMotivators = firebaseAction(({ bindFirebaseRef }, ref) => {
  console.log('[actions] Motivators ref set')
  bindFirebaseRef('motivators', ref)
})

export const loadDemotivators = firebaseAction(({ bindFirebaseRef }, ref) => {
  console.log('[actions] Demotivators ref set')
  bindFirebaseRef('demotivators', ref)
})

export const loadProjects = firebaseAction(({ bindFirebaseRef, state }) => {
  console.log('[actions] loading Projects')
  bindFirebaseRef(
    'projects',
    DB.collection(`projects`)
      .where('ownerId', '==', state.user.uid)
      .orderBy('order', 'asc')
  )
})

export const loadOrgProjects = firebaseAction(({ bindFirebaseRef, state }) => {
  console.log('[actions] loading Org Projects')
  bindFirebaseRef(
    'orgProjects',
    DB.collection(`projects`)
      .where('sharedWithArr', 'array-contains', `${state.user.email}`)
  )
})

export function getUserProfile ({ state }, playbook) {
  return DB
    .doc(`users/${state.user.uid}`)
    .get()
}
export const getSomeUserPlaybook = firebaseAction(({ bindFirebaseRef }, uid) => {
  console.log('get some user\'s playbook. UID is: ' + uid)
  return DB
    .doc(`users/${uid}`)
    .get()
})

// user profile actions
export function updatePlaybook ({ state }, playbook) {
  console.log('[actions] updating playbook')
  let newPlaybook = Object.assign(state.userProfile.playbook || {}, playbook)
  DB
    .doc(`users/${state.user.uid}`)
    .update({ playbook: newPlaybook })
}

export function updateAttitude ({ state }, { attitude, teamId, memberId }) {
  DB
    .doc(getMemberURL(state, teamId, memberId))
    .update({ attitude })
}

export function updateDelegation ({ state }, { delegation, teamId, memberId }) {
  DB
    .doc(getMemberURL(state, teamId, memberId))
    .update({ delegation })
}

// Projects CRUD actions
export function addProject ({ state }, project) {
  console.log('new project added')
  DB
    .collection(`projects`)
    .add(Object.assign(project, {
      ownerId: state.user.uid,
      createdAt: Date.now()
    }))
}

export function updateProject ({ state, dispatch }, project) {
  console.log('project %s updated', project.id)
  DB.doc(`projects/${project.id}`).update(project)
}

export function updateMultipleProjects ({ state }, projects) {
  console.info('[Actions] mutliple projects updated')
  let batch = DB.batch()
  projects.forEach(project => {
    let docRef = DB.doc(`projects/${project.id}`)
    batch.update(docRef, project)
  })
  batch.commit()
}

export function addMemberEmailToSharedProjects ({getters, dispatch}, member) {
  console.info(`[Actions] shared projects updated`)
  let sharedProjects = getters.getMySharedProjects()
  sharedProjects.forEach(project => {
    (project.sharedWithArr || []).push(member.email)
  })
  dispatch('updateMultipleProjects', sharedProjects)
}

export function removeMemberEmailFromSharedProjects ({getters, dispatch}, member) {
  console.info(`[Actions] shared projects updated`)
  let sharedProjects = getters.getMySharedProjects()
  sharedProjects.forEach(project => {
    const indexOfEmail = (project.sharedWithArr || []).indexOf(member.email);
    (project.sharedWithArr || []).splice(indexOfEmail, 1)
  })
  dispatch('updateMultipleProjects', sharedProjects)
}

export function deleteProject ({ state }, project) {
  console.log('project %s deleted', project.id)
  DB.doc(`users/${state.user.uid}/projects/${project.id}`).delete()
}

// Teams CRUD actions
export function addTeam ({ state }, team) {
  team = Object.assign(state.models.team, team)
  team.createdAt = Date.now()
  return DB.collection(`users/${state.user.uid}/teams`).add(team)
}

export function updateTeam ({ state }, team) {
  DB.doc(`users/${state.user.uid}/teams/${team.id}`).update(team)
}

export function deleteTeam ({ state }, team) {
  DB.doc(`users/${state.user.uid}/teams/${team.id}`).delete()
}

export async function loadTeam ({state, commit}, teamId) {
  console.log(`[Action] loading team: ${teamId}`)
  const doc = await DB.doc(`users/${state.user.uid}/teams/${teamId}`).get()
  if (doc.exists) {
    commit('setCurrentTeam', { id: teamId, ...doc.data() })
  }
}

// members CRUD actions
export function addMember ({ state }, { teamId, member }) {
  console.log('adding new member to team: ', teamId)
  member = Object.assign(state.models.member, member)
  member.createdAt = Date.now()
  return DB.collection(`users/${state.user.uid}/teams/${teamId}/members`).add(member)
}

export function updateMember ({ state, dispatch }, { teamId, member }) {
  console.log(`updating member ${member.id} in team: ${teamId}`)
  DB.doc(getMemberURL(state, teamId, member.id)).update(member)
    .catch(() => {
      DB.doc(getMemberURL(state, teamId, member.id)).set(member)
    })
  dispatch('addMemberEmailToSharedProjects', member)
}

export function deleteMember ({ state, dispatch }, { teamId, member }) {
  console.log(`deleting member ${member.id} from team: ${teamId}`)
  DB.doc(`users/${state.user.uid}/teams/${teamId}/members/${member.id}`).delete()
  dispatch('removeMemberEmailFromSharedProjects', member)
}

export async function loadMember ({state, commit}, {teamId, memberId}) {
  console.log(`[Actions] Loading member: ${memberId} of team: ${teamId}`)
  const doc = await DB.doc(`users/${state.user.uid}/teams/${teamId}/members/${memberId}`).get()
  if (doc.exists) {
    commit('setCurrentMember', { id: memberId, ...doc.data() })
  }
}

export async function loadAllMembers ({ state, commit }) {
  console.log(`[Vuex Actions] Getting all members`)
  if (state.teams.length > 0) {
    let promises = []
    let allMembers = []
    state.teams.forEach((team) => {
      promises.push(DB.collection(`users/${state.user.uid}/teams/${team.id}/members`).get())
    })
    const snapshots = await Promise.all(promises)
    snapshots.forEach((snapshot, i) => {
      snapshot.forEach(doc => {
        allMembers.push({
          id: doc.id,
          teamId: state.teams[i].id,
          teamName: state.teams[i].name,
          ...doc.data()
        })
      })
    })
    commit('setAllMembers', allMembers)
  } else {
    console.error('Teams are not loaded yet. Loading all members failed')
  }
}

// Goals CRUD actions
export function addGoal ({ state }, { teamId, memberId, goal }) {
  console.log(`new goal added to member: ${memberId} in team: ${teamId}`)
  DB.collection(`${getMemberURL(state, teamId, memberId)}/goals`).add(goal)
}

export function deleteGoal ({ state }, { teamId, memberId, goalId }) {
  DB.doc(`${getMemberURL(state, teamId, memberId)}/goals/${goalId}`).delete()
}

export function updateGoal ({ state }, { teamId, memberId, goal }) {
  DB.doc(`${getMemberURL(state, teamId, memberId)}/goals/${goal.id}`).update(goal)
}

// Strength CRUD actions
export function addStrength ({ state }, { teamId, memberId, strength }) {
  DB.collection(`${getMemberURL(state, teamId, memberId)}/strengths`).add(strength)
}

export function deleteStrength ({ state }, { teamId, memberId, strengthId }) {
  DB.doc(`${getMemberURL(state, teamId, memberId)}/strengths/${strengthId}`).delete()
}

export function updateStrength ({ state }, { teamId, memberId, strength }) {
  console.log('update strength ' + strength.id)
  DB
    .doc(`${getMemberURL(state, teamId, memberId)}/strengths/${strength.id}`)
    .update(strength)
}

// Motivator CRUD actions
export function addMotivator ({ state }, { teamId, memberId, motivator }) {
  console.log('adding motivator ...', memberId)
  DB
    .doc(`${getMemberURL(state, teamId, memberId)}/motivators/${motivator.id}`)
    .set(motivator)
}

export function deleteMotivator ({ state }, { teamId, memberId, motivatorId }) {
  DB.doc(`${getMemberURL(state, teamId, memberId)}/motivators/${motivatorId}`).delete()
}

export function updateMotivator ({ state }, { teamId, memberId, motivator }) {
  console.log('update motivator ' + motivator.id)
  DB
    .doc(`${getMemberURL(state, teamId, memberId)}/motivators/${motivator.id}`)
    .update(motivator)
}

// Demotivator CRUD actions
export function addDemotivator ({ state }, { teamId, memberId, demotivator }) {
  console.log('adding demotivator ...')
  DB
    .doc(`${getMemberURL(state, teamId, memberId)}/demotivators/${demotivator.id}`)
    .set(demotivator)
}

export function deleteDemotivator ({ state }, { teamId, memberId, demotivatorId }) {
  DB.doc(`${getMemberURL(state, teamId, memberId)}/demotivators/${demotivatorId}`).delete()
}

export function updateDemotivator ({ state }, { teamId, memberId, demotivator }) {
  console.log('update demotivator ' + demotivator.id)
  DB
    .doc(`${getMemberURL(state, teamId, memberId)}/demotivators/${demotivator.id}`)
    .update(demotivator)
}

function getMemberURL (state, teamId, memberId) {
  let memberURL = ''
  if (state.isUserProfileEditing) {
    memberURL = `users/${state.user.uid}`
  } else {
    memberURL = `users/${state.user.uid}/teams/${teamId}/members/${memberId}`
  }
  return memberURL
}

// get playbook data
export function getPlaybookByUser ({ state, dispatch, commit }, uid) {
  console.log('dispatch called ', uid)
  // uid.uid = "mr2RYgaQQJZ5eXm5TcJ86oOf9Zu2" // "JUdY35r0UYhDqFjXFyEYducaHXh2"
  dispatch('setPlaybook', uid)
}

export const setPlaybookRef = firebaseAction(({ bindFirebaseRef }, ref) => {
  console.log('[actions] Plabybook ref set')
  bindFirebaseRef('playbook', ref)
})

export function getExistOrgCode ({ state }, code) {
  // console.log('code :', code)
  return DB
    .collection(`users`)
    .where('playbook.basics.orgCode', '==', code)
    .get()
}
