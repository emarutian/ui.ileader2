export function setUserProfile (state, userProfile) {
  state.userProfile = userProfile
}
export function setSomeUserPlaybook (state, playbook) {
  state.someUserPlaybook = playbook
}

export function setAllMembers (state, allMembers) {
  state.allMembers = allMembers
}

export function showLayoutToolbar (state) {
  state.layoutToolbarVisible = true
}

export function hideLayoutToolbar (state) {
  state.layoutToolbarVisible = false
}

export function enableEditMode (state) {
  state.editMode = true
}

export function disableEditMode (state) {
  state.editMode = false
}

export function setCurrentTeam (state, currentTeam) {
  state.team = currentTeam
}

export function setCurrentMember (state, currentMember) {
  state.member = currentMember
}

export function enableUserProfileEditing (state) {
  state.isUserProfileEditing = true
}

export function disableUserProfileEditing (state) {
  state.isUserProfileEditing = false
}

export function setUserPlaybook (state, playbook) {
  state.playbook = playbook
}
