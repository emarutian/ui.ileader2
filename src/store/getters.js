export default {
  user: (state, getters) => () => {
    return state.user
  },
  org: (state, getters) => () => {
    return state.org
  },
  getTeams: (state, getters) => (query = '') => {
    if (query) {
      query = query.toLowerCase()
      return state.teams.filter(team => {
        return team.name.toLowerCase().includes(query)
      })
    } else {
      return state.teams
    }
  },
  getAllMembers: (state, getters) => (query = '') => {
    if (query) {
      query = query.toLowerCase()
      return state.allMembers.filter(member => {
        return (
          String(member.first_name).toLowerCase().includes(query) ||
          String(member.last_name).toLowerCase().includes(query) ||
          String(member.strengthsSummary).toLowerCase().includes(query) ||
          String(member.role).toLowerCase().includes(query)
        )
      })
    } else {
      return state.allMembers
    }
  },

  getAllMembersEmails: (state, getters) => () => {
    return state.allMembers.map(member => {
      return member.email
    })
  },
  getPlaybookStatus: (state, getters) => () => {
    var total = 0
    const userProfile = state.userProfile
    if (!userProfile) {
      return total
    }
    const playbook = userProfile.playbook
    if (playbook) {
      const basics = playbook.basics ? playbook.basics.percComplete : 0
      const purpose = playbook.purpose ? playbook.purpose.percComplete : 0
      const products = playbook.products ? playbook.products.percComplete : 0
      const focus = playbook.focus ? playbook.focus.percComplete : 0
      const strategy = playbook.strategy ? playbook.strategy.percComplete : 0
      const values = playbook.values ? playbook.values.percComplete : 0
      const decisionAreas = playbook.decisionAreas ? playbook.decisionAreas.percComplete : 0
      total = Math.round((basics + purpose + products + focus + strategy + values + decisionAreas) / 7)
      total = total > 98 ? 100 : total
    }
    return total
  },
  getProjects: (state, getters) => ({ header, important, urgent, delegated, shared, completed } = {}) => {
    let filteredProjects = state.projects
    if (!completed) {
      filteredProjects = filteredProjects.filter(project => project.is_completed === false)
    }
    if (completed) {
      filteredProjects = filteredProjects.filter(project => project.is_completed === true)
    }
    filteredProjects = filteredProjects.filter(project =>
      project.ownerId === state.user.uid
    )
    if (header) {
      filteredProjects = filteredProjects.filter(project =>
        project.header.toLowerCase().includes(header.toLowerCase())
      )
    }
    if (important) {
      filteredProjects = filteredProjects.filter(project => project.is_important === important)
    }
    if (urgent) {
      filteredProjects = filteredProjects.filter(project => project.is_urgent === urgent)
    }
    if (delegated) {
      filteredProjects = filteredProjects.filter(project => project.is_delegated === delegated)
    }
    if (shared) {
      filteredProjects = filteredProjects.filter(project => project.is_shared === shared)
    }
    return filteredProjects
  },

  getMySharedProjects: (state) => () => {
    return state.projects.filter(project => {
      return project.is_shared
    })
  },

  getOrgProjects: (state) => () => {
    return state.orgProjects.filter(project => {
      return project.ownerId !== state.user.uid
    })
  },

  defaultMembers: (state, getters) => ({ name } = {}) => {
    if (name) {
      name = name.toLowerCase()
      return state.members.filter(member => {
        return member.first_name.toLowerCase().includes(name) || member.last_name.toLowerCase().includes(name)
      })
    } else {
      return state.members
    }
  },

  getGoals: (state) => ({ isArchived } = {isArchived: false}) => {
    return state.goals.filter(goal => {
      return (goal.is_archived === isArchived) || (goal.is_archived === false)
    })
  },

  members: (state, getters) => (teamid) => {
    return state.members.filter(t => t.team_id === teamid && t.is_deleted === false)
  },

  percComplete: (state, getters) => (memberId, type) => {
    let collection = String(type).toLowerCase() + 's'
    let total = state[collection]
    if (total.length === 0) {
      return 0
    }
    var addressed = total.filter(i => i.is_addressed === true)
    return Math.floor(addressed.length * 100 / total.length)
  },
  memberNeedsAttn: (state, getters) => (memberId, type) => {
    var items
    if (type === 'ALL') {
      items = state.chars.filter(i => i.user_id === memberId)
    } else {
      items = state.chars.filter(i => i.user_id === memberId && i.type === type)
    }

    if (items.length === 0) {
      return true
    }
    var addressed = items.filter(i => i.is_addressed === false)
    return (addressed.length > 0)
  },

  charsByMemberIdAndType: (state, getters) => (memberId, type) => {
    let collection = String(type).toLowerCase() + 's'
    return state[collection]
  },

  getAssigneesSearchResult: (state, getters) => (terms, done) => {
    const members = state.allMembers
    const teams = state.teams
    let assigneeOptions = []

    assigneeOptions = assigneeOptions.concat(members.map((member) => {
      return {
        label: `${member.first_name} ${member.last_name}`,
        value: member.id,
        stamp: 'Member',
        teamId: member.teamId
      }
    }))
    assigneeOptions = assigneeOptions.concat(teams.map((team) => {
      return {
        label: `${team.name}`,
        value: team.id,
        stamp: 'Team'
      }
    }))
    assigneeOptions = assigneeOptions.filter(item => {
      return item.label.toLowerCase().includes(terms.toLowerCase())
    })
    if (assigneeOptions.length === 0) {
      assigneeOptions = getters.getDefaultAutocompleteOptions(terms)
    }
    done(assigneeOptions)
  },

  getDefaultAutocompleteOptions: (state) => (terms) => {
    const teams = state.teams
    const newMembers = teams.map(team => {
      return {
        label: `Create New Member "${terms}"`,
        value: 'ADD_MEMBER',
        name: terms,
        stamp: `@${team.name}`,
        teamId: team.id
      }
    })
    return [
      {
        label: `Create New Team "${terms}"`,
        value: 'ADD_TEAM',
        name: terms
      },
      ...newMembers
    ]
  }
}
