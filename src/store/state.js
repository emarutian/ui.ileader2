// import Abilities from '../constants/Abilities.json'
// import AbilityLevels from '../constants/AbilityLevels.json'

export default {
  // abilities: Abilities,
  // abilityLevels: AbilityLevels,
  layoutToolbarVisible: true,
  editMode: false,
  isUserProfileEditing: false,
  teamsFetchCompleted: false,

  // org details
  projects: [],
  orgProjects: [],
  teams: [],
  members: [],
  allMembers: [],

  // current
  user: null,
  userProfile: {},
  org: {},
  team: {},
  member: {},
  playbook: {},
  // some other user's playbook this user can see
  someUserPlaybook: {},

  // profile details
  goals: [],
  strengths: [],
  motivators: [],
  demotivators: [],
  chars: [],

  // models
  models: {
    org: {
      ownerId: '',
      extrinsicGoal: '',
      autonomousGoal: '',
      goalsAreAligned: false,
      isSmall: false,
      hasTrust: false,
      masteredConflict: false,
      embracedAccountability: false,
      focusedOnResults: false,
      achivesCommitments: false
    },
    team: {
      extrinsicGoal: '',
      autonomousGoal: '',
      goalsAreAligned: false,
      isSmall: false,
      hasTrust: false,
      masteredConflict: false,
      embracedAccountability: false,
      focusedOnResults: false,
      achivesCommitments: false
    },
    member: {
      first_name: '',
      last_name: '',
      role: '',
      email: '',
      notes: '',
      personality_type: '',
      is_deleted: false,
      is_invited: false,
      advanced_mode: false
    }
  }
}
