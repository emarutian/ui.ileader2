
export default ({ Vue }) => {
  Vue.filter('replaceAll', function (block, search, replacement) {
    if (block) {
      return block.split(search).join(replacement)
    } else {
      return block
    }
  })

  Vue.filter('csv', function (chars) {
    var result = ''
    chars.forEach(function (i) {
      result = result + i.name + '; '
    })
    return result
  })

  Vue.filter('pluralizeCharType', function (str) {
    switch (str) {
      case 'ABILITY':
        return 'Abilities'
      case 'MOTIVATOR':
        return 'Motivators'
      case 'DEMOTIVATOR':
        return 'Demotivators'
      default:
    }
  })
}
