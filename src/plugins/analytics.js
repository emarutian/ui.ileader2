import VueAnalytics from 'vue-analytics'

export default ({ router, Vue }) => {
  Vue.prototype.$ga = VueAnalytics

  Vue.use(VueAnalytics, {
    id: 'UA-96108484-2',
    router,
    autoTracking: {
      pageviewTemplate (route) {
        return {
          page: route.path,
          title: document.title + '( ' + route.name + ' )',
          location: window.location.href
        }
      }
    },
    checkDuplicatedScript: true
  })
}
