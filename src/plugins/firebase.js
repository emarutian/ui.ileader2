import firebase from 'firebase'

// required for side effects
require('firebase/functions')

const config = {
  apiKey: 'AIzaSyB7AwnBoOLpqIPDecT3sCDf1ZjxExD__6o',
  authDomain: 'better-leader.firebaseapp.com',
  databaseURL: 'https://better-leader.firebaseio.com',
  projectId: 'better-leader',
  storageBucket: 'better-leader.appspot.com',
  messagingSenderId: '613276337852'
}

export const fireApp = firebase.initializeApp(config)

const firestore = fireApp.firestore()

export const AUTH = fireApp.auth()
export const DB = firestore
export const FUNCTIONS = fireApp.functions()

export default ({ app, router, Vue }) => {
  Vue.prototype.$auth = AUTH
  Vue.prototype.$db = DB
  Vue.prototype.$functions = FUNCTIONS
}
