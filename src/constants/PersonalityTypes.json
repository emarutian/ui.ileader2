[
  {
    "code": "ACHIEVEMENT",
    "name": "Achievement",
    "description": "_NAME_'s excitement about the job increases as responsibilities grow. _NAME_'s a risk taker who doesn't care much about recognition. _NAME_ gets highs from the satisfaction of giving superior performance. Cream puff work that is repetitive or menial is frustrating and greatly decreases the effort _NAME_ gives to the job. Given the choice of working in a group or sole, _NAME_ will always choose going solo unless _NAME_ is partnered with someone who is similar minded.",
    "analysis": "People like _NAME_, with a high need for achievement, seek to excel. Predominantly achievement-motivated individuals avoid low-risk situations because the easily attained success is not a genuine achievement. These individuals prefer work that has a moderate probability of success. Achievement-motivated individuals want regular feedback in order to monitor the progress of their achievements. They prefer to work either alone or with others like themselves"
  },
  {
    "code": "POWER",
    "name": "Power",
    "description": "Whether it pertains to own work area and product or coworkers, _NAME_ needs power, usually in the form of control. To some people, _NAME_ can come off as bossy and territorial, but _NAME_'s very competent. When _NAME_'s in control, _NAME_'s highly motivated. Loss of the control, leads to loss of desire to work for the company.",
    "analysis": "Folks like _NAME_ need personal power. They want to be in charge, and they crave the authority to make decisions that will impact others. The need for power also means wanting to be well regarded and to be followed. Power-motivated individuals typically do not respond well to being told what to do or how to do it'unless it comes from a person they wish to emulate"
  },
  {
    "code": "AFFILIATION",
    "name": "Affiliation",
    "description": "Everyone knows _NAME_; _NAME_ is a team player through and through. Nothing motivates _NAME_ to improve organizational systems, solve problems, and find new opportunities more than working in a group environment. _NAME_ has got a gift for infusing both coworkers and clients with positive energy and for pulling people together to achieve a common goal. If you give _NAME_ a project where _NAME_ might step on some of the coworkers' feet, don't be surprised if _NAME_ passes.",
    "analysis": "Employees like _NAME_ have a high need for affiliation. They want harmonious relationships, and they want to feel accepted by other people. These individuals prefer work that provides significant personal interaction. They enjoy being part of groups and make excellent team members, though sometimes are distractible into social interaction. They can perform particularly well in customer service and client interaction situations"
  },
  {
    "code": "SECURITY",
    "name": "Security",
    "description": "_NAME_ is driven by having a clear job role and taking on only sure bets. _NAME_ doesn't want rapid change or high-risk opportunities. If it hasn't been done before or if _NAME_ can't see it in writing, _NAME_ is not interested. If you invite _NAME_ in on a project by saying, 'I'm not sure how you fit into this team yet, but come on board and we'll figure it out as we go,' you're going to send _NAME_ scurrying for safety.",
    "analysis": "_NAME_ and others like him have a high need for security and look for continuity in their work life. They may prefer to stay with the same company, or in the same position or department, for the long haul. They are driven by guarantees. High-security people get anxious over change. They value consistency in their job, work, and pay."
  },
  {
    "code": "REWARD",
    "name": "Reward",
    "description": "_NAME_ works efficiently and produces the best results when _NAME_ knows what's in it for _NAME_. If you put _NAME_ on a team with coworkers who don't pull their load but will get the same reward, _NAME_’s motivation decreases rapidly.",
    "analysis": "Employees like _NAME_, who have a high need for reward, are looking for the tangibles they can accumulate through their work. They want to know how much they can earn and how they can earn it. They want to know how they will be compensated for their time and effort. And they need to have it spelled out clearly. High-reward people like to see that effort and compensation are clearly aligned. They typically do not like systems that reward time in a job over effort in a job."
  },
  {
    "code": "ADVENTURE",
    "name": "Adventure",
    "description": "_NAME_ is easily bored. _NAME_ is an adrenaline junkie and needs to do interesting and cutting-edge work. If there's a new or experimental project, _NAME_ is all over it. _NAME_ doesn't much care for stupid people and thus often works solo. Give _NAME_ a risky project with lots of autonomy, and _NAME_ is ecstatic. Put _NAME_ with a large group of mediocre minds doing boring work, and _NAME_ is ready to take a header off their cubicle wall.",
    "analysis": "People like _NAME_ have a need for adventure and are motivated by risk, change, and uncertainty. They thrive when the environment or the work is constantly changing. They tend to like challenges and jump at the opportunity to be the first to do something new. They don't mind failure, especially if given the chance to try again. High-adventure people often go out on their own. They may be entrepreneurs or freelancers. They are likely to change jobs and companies often, especially when they get bored or feel that they have maxed out their potential somewhere."
  },
  {
    "code": "ACTUALIZATION",
    "name": "Actualization",
    "description": "_NAME_ is driven by a need to feel good about self and the work _NAME_ does. Last month _NAME_ worked on a project that developed creative ways to increase teaching about healthy living and eating in the public school system, and _NAME_ was more driven and productive than _NAME_ had ever been.",
    "analysis": "Folks like _NAME_, who have a need for actualization, focus on a desire for self-fulfillment, namely the tendency to reach their own greatest potential. They want to maximize themselves in the world through their job. High-actualization people tend to concentrate more on their own goals than on the goals of a company although those goals can be aligned."
  }
]