-- basic query to set parent value
update content_model_reference set parent_id = left(element_id, length(`element_id`)-length(SUBSTRING_INDEX(element_id, '.', -1))+1);
-- query to identify which nodes are parents
update content_model_reference as c inner join (select left(element_id, length(`element_id`)-2) as parent, element_id as oid, parent_id from content_model_reference) as x on c.element_id = x.parent set c.is_parent = true;
-- basic select query to transform to JSON format
select element_id as code, left(element_id, length(`element_id`)-2) as parent_id, element_name as name, is_parent, false as selected, description from content_model_reference where element_id like '1%' or element_id like '2%';