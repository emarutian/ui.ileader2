export const SET_USER = 'SET_USER'
export const SET_ORG = 'SET_ORG'
export const SET_MEMBERS = 'SET_MEMBERS'
export const SET_MEMBER = 'SET_MEMBER'
export const SET_CHARS = 'SET_CHARS'

export const ADD_TEAM = 'ADD_TEAM'
export const EDIT_TEAM = 'EDIT_TEAM'
export const DELETE_TEAM = 'DELETE_TEAM'

export const ADD_MEMBER = 'ADD_MEMBER'
export const EDIT_MEMBER = 'EDIT_MEMBER'
export const DELETE_MEMBER = 'DELETE_MEMBER'

export const LOAD_MEMBER_GOALS = 'LOAD_MEMBER_GOALS'
export const LOAD_GOAL = 'LOAD_GOAL'
export const ADD_GOAL = 'ADD_GOAL'
export const EDIT_GOAL = 'EDIT_GOAL'
export const DELETE_GOAL = 'DELETE_GOAL'

export const ADD_CHAR = 'ADD_CHAR'
export const EDIT_CHAR = 'EDIT_CHAR'
export const DELETE_CHAR = 'DELETE_CHAR'
export const SYNC_CHARS = 'SYNC_CHARS'
